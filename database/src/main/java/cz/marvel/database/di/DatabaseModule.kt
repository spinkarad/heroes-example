package cz.marvel.database.di

import android.content.Context
import androidx.room.Room
import cz.marvel.database.HeroesDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module(override = true) {

    // Database
    single { createDatabase(androidApplication()) }

    // Daos
    single { get<HeroesDatabase>().heroesDao() }
}

private fun createDatabase(context: Context): HeroesDatabase {
    return Room.databaseBuilder(context, HeroesDatabase::class.java, "hero_db")
        .fallbackToDestructiveMigration()
        .build()
}