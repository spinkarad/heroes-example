package cz.marvel.database

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.marvel.basedata.entities.Hero
import cz.marvel.database.daos.HeroesDao


@Database(
    entities = [Hero::class],
    version = 1,
    exportSchema = true
)
abstract class HeroesDatabase : RoomDatabase(){
    abstract fun heroesDao(): HeroesDao
}
