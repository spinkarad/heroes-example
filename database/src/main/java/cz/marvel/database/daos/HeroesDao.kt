package cz.marvel.database.daos

import androidx.room.Dao
import androidx.room.Query
import cz.marvel.basedata.entities.Hero
import kotlinx.coroutines.flow.Flow

@Dao
abstract class HeroesDao : EntityDao<Hero>() {

    @Query("SELECT * FROM Hero")
    abstract fun getHeroes(): Flow<List<Hero>>

    @Query("SELECT * FROM Hero WHERE id=:id")
    abstract fun getHero(id:String): Flow<Hero>
}