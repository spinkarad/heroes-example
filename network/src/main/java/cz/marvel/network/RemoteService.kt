package cz.marvel.network

import cz.marvel.network.data.HeroesResponse
import retrofit2.http.GET

interface RemoteService {

    @GET("characters")
    suspend fun getHeroes(): HeroesResponse
}