package cz.marvel.network.di

import cz.marvel.network.AuthInterceptor
import cz.marvel.network.RemoteService
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module(override = true) {
    single { createOkHttpClient() }
    single { createRemoteService(get()) }
}

private val authInterceptor = AuthInterceptor()

private fun createRemoteService(client: OkHttpClient): RemoteService =
    Retrofit.Builder()
        .baseUrl("http://gateway.marvel.com/v1/public/")
        .addConverterFactory(MoshiConverterFactory.create())
        .client(client)
        .build().create((RemoteService::class.java))


private fun createOkHttpClient(): OkHttpClient =
    OkHttpClient.Builder().apply {
        connectTimeout(10, TimeUnit.SECONDS)
        readTimeout(20, TimeUnit.SECONDS)
        writeTimeout(20, TimeUnit.SECONDS)
        addInterceptor(authInterceptor)
    }.build()
