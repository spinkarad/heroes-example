package cz.marvel.network

import okhttp3.Interceptor
import okhttp3.Response
import java.math.BigInteger
import java.security.MessageDigest


class AuthInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val currentMillis = "${System.currentTimeMillis()}"
        val hash = (currentMillis + BuildConfig.PRIVATE_KEY + BuildConfig.PUBLIC_KEY).md5()

        val origRequest = chain.request()
        val url = origRequest.url.newBuilder()
            .addQueryParameter("ts", currentMillis)
            .addQueryParameter("apikey", BuildConfig.PUBLIC_KEY)
            .addQueryParameter("hash", hash)
            .build()

        return origRequest.newBuilder().url(url).build().let(chain::proceed)
    }

    private fun String.md5(): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
    }
}