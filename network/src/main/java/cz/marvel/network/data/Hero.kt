package cz.marvel.network.data

data class HeroesResponse(val data: HeroesContainerDto)

data class HeroesContainerDto(val results: List<HeroDto>)

data class HeroDto(val name: String, val id: String, val thumbnail: ThumbnailDto, val description: String)

data class ThumbnailDto(val path: String, val extension:String)