package cz.marvel.featuredetail.di

import cz.marvel.data.di.detailDataModule
import cz.marvel.featuredetail.ui.HeroDetailViewModel
import org.koin.androidx.experimental.dsl.viewModel
import org.koin.dsl.module

val detailModule = module {
    viewModel<HeroDetailViewModel>()
} + detailDataModule