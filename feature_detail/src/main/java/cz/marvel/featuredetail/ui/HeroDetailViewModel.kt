package cz.marvel.featuredetail.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cz.marvel.basedata.entities.Hero
import cz.marvel.data.hero.HeroRepository
import kotlinx.coroutines.flow.collect

class HeroDetailViewModel(private val heroRepo: HeroRepository) : ViewModel() {

    fun getHero(heroId: String): LiveData<Hero> = liveData {
        heroRepo.observe(heroId).collect (::emit)
    }
}