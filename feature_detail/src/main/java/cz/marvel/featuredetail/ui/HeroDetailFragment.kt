package cz.marvel.featuredetail.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import coil.load
import cz.marvel.featuredetail.R
import kotlinx.android.synthetic.main.fragament_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HeroDetailFragment : Fragment(R.layout.fragament_detail) {
    private val viewModel by viewModel<HeroDetailViewModel>()
    private val args by navArgs<HeroDetailFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeHero(args.heroId)
    }

    private fun observeHero(heroId: String) =
        viewModel.getHero(heroId).observe(viewLifecycleOwner) { hero ->
            heroImage.load(hero.imageUrl)
            heroName.text = hero.name
            heroDescription.text = hero.description
        }
}