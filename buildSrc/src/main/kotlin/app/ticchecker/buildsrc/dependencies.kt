package app.ticchecker.buildsrc

object Libs {

    const val androidGradlePlugin = "com.android.tools.build:gradle:4.0.1"

    object Kotlin {
        private const val version = "1.3.72"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
        const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"

        object Coroutines {
            private const val version = "1.3.9"
            const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
            const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
        }
    }

    object AndroidX {
        const val appcompat = "androidx.appcompat:appcompat:1.2.0"
        const val coreKtx = "androidx.core:core-ktx:1.3.1"
        const val fragmentKtx = "androidx.fragment:fragment-ktx:1.2.5"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.0.0"

        object LifeCycle {
            const val liveDataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:2.2.0"
        }

        object Navigation {
            private const val version = "2.3.0"
            const val fragment = "androidx.navigation:navigation-fragment-ktx:$version"
            const val ui = "androidx.navigation:navigation-ui-ktx:$version"
            const val safeArgs = "androidx.navigation:navigation-safe-args-gradle-plugin:$version"
        }

        object Room {
            const val version = "2.2.5"
            const val runtime = "androidx.room:room-runtime:$version"
            const val compiler = "androidx.room:room-compiler:$version"
            const val ktx = "androidx.room:room-ktx:$version"
        }
    }

    object Google {
        const val material = "com.google.android.material:material:1.2.0"
        const val gmsGoogleServices = "com.google.gms:google-services:4.3.3"
    }

    object Koin {
        private const val version = "2.1.6"
        const val core = "org.koin:koin-core:$version"
        const val scope = "org.koin:koin-androidx-scope:$version"
        const val viewModel = "org.koin:koin-androidx-viewmodel:$version"
        const val experimental = "org.koin:koin-androidx-ext:$version"
    }

    object Square {
        object Retrofit {
            private const val version = "2.9.0"
            const val core = "com.squareup.retrofit2:retrofit:$version"
            const val moshi = "com.squareup.retrofit2:converter-moshi:$version"
        }

        object OkHttp {
            private const val version = "4.8.1"
            const val core = "com.squareup.okhttp3:okhttp:$version"
            const val logInterceptor = "com.squareup.okhttp3:logging-interceptor:$version"
        }
    }

    const val coil = "io.coil-kt:coil:1.0.0-rc1"
}