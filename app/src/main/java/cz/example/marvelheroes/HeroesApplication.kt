package cz.example.marvelheroes

import android.app.Application
import cz.marvel.collection.di.collectionModule
import cz.marvel.featuredetail.di.detailModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class HeroesApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() =
        startKoin {
            androidContext(this@HeroesApplication)
            modules(collectionModule + detailModule)
        }
}