package cz.marvel.basedata.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Hero(
    @PrimaryKey val id: String,
    val name: String,
    val imageUrl: String,
    val description: String
)