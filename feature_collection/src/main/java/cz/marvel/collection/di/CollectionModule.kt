package cz.marvel.collection.di

import cz.marvel.collection.ui.HeroCollectionViewModel
import cz.marvel.collection.ui.HeroesAdapter
import cz.marvel.data.di.collectionDataModule
import org.koin.androidx.experimental.dsl.viewModel
import org.koin.dsl.module
import org.koin.experimental.builder.single

val collectionModule = module {

    viewModel<HeroCollectionViewModel>()
    single<HeroesAdapter>()
} + collectionDataModule