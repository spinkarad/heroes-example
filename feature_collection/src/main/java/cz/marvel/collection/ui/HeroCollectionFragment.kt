package cz.marvel.collection.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import cz.marvel.collection.R
import kotlinx.android.synthetic.main.fragment_collection.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class HeroCollectionFragment : Fragment(R.layout.fragment_collection) {
    private val viewModel by viewModel<HeroCollectionViewModel>()
    private val heroesAdapter by inject<HeroesAdapter>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        observeHeroes()
    }

    private fun setUpRecyclerView() = heroesRecyclerView.run {
        adapter = heroesAdapter.apply { setOnClickListener { navigateToDetail(it.id) } }
    }

    private fun observeHeroes() =
        viewModel.heroes.observe(viewLifecycleOwner, heroesAdapter::setHeroes)

    private fun navigateToDetail(heroId: String) =
        findNavController().navigate(HeroCollectionFragmentDirections.fromCollectionToDetail(heroId))
}

