package cz.marvel.collection.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cz.marvel.basedata.entities.Hero
import cz.marvel.data.heroes.HeroesRepository
import kotlinx.coroutines.flow.collect

internal class HeroCollectionViewModel(private val heroesRepo: HeroesRepository) :
    ViewModel() {

    val heroes: LiveData<List<Hero>> = liveData {
        heroesRepo.observe().collect (::emit)
    }

}