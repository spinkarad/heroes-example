package cz.marvel.collection.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import cz.marvel.basedata.entities.Hero
import cz.marvel.collection.R
import kotlinx.android.synthetic.main.layout_hero_list_item.view.*

class HeroesAdapter : RecyclerView.Adapter<HeroesAdapter.MyViewHolder>() {

    private var heroes: List<Hero> = emptyList()
    private var onClickListener: ((hero: Hero) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_hero_list_item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(heroes[position])
    }

    override fun getItemCount(): Int = heroes.size

    fun setOnClickListener(listener: (hero: Hero) -> Unit) {
        this.onClickListener = listener
    }

    fun setHeroes(heroes: List<Hero>) {
        this.heroes = heroes
        notifyDataSetChanged()
    }

    inner class MyViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(hero: Hero) =
            itemView.run {
                setOnClickListener { onClickListener?.invoke(hero) }
                thumbImageView.load(hero.imageUrl) { crossfade(true) }
                nameTextView.text = hero.name
            }
    }
}
