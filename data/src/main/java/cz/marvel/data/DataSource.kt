package cz.marvel.data

interface DataSource<out T> {
    suspend fun getData(): T
}
