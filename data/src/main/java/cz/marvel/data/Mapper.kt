package cz.marvel.data

interface Mapper<F, T> {
    suspend fun map(from: F): T
}

suspend fun <F, T> Mapper<F, T>.forLists(from: List<F>): List<T> {
    return from.map { item -> map(item) }
}
