package cz.marvel.data.di

import cz.marvel.data.hero.DefaultHeroRepository
import cz.marvel.data.hero.HeroDataStore
import cz.marvel.data.hero.HeroRepository
import cz.marvel.data.heroes.DefaultHeroesRepository
import cz.marvel.data.heroes.HeroesDataSource
import cz.marvel.data.heroes.HeroesDataStore
import cz.marvel.data.heroes.HeroesRepository
import cz.marvel.data.heroes.mappers.HeroDtoToHeroMapper
import cz.marvel.data.heroes.mappers.HeroesResponseToHeroesListMapper
import cz.marvel.database.di.databaseModule
import cz.marvel.network.di.networkModule
import org.koin.dsl.module
import org.koin.experimental.builder.single
import org.koin.experimental.builder.singleBy

val collectionDataModule = module {
    singleBy<HeroesRepository, DefaultHeroesRepository>()
    single<HeroesDataSource>()
    single<HeroesDataStore>()
    single<HeroDtoToHeroMapper>()
    single<HeroesResponseToHeroesListMapper>()
} + networkModule + databaseModule


val detailDataModule = module {
    singleBy<HeroRepository, DefaultHeroRepository>()
    factory{ (heroId: String) -> HeroDataStore(get(), heroId) }
} + networkModule + databaseModule