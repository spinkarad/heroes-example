package cz.marvel.data.heroes

import cz.marvel.basedata.entities.Hero
import kotlinx.coroutines.flow.Flow

interface HeroesRepository {
    suspend fun observe(): Flow<List<Hero>>
    suspend fun refresh()
}