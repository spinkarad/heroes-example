package cz.marvel.data.heroes.mappers

import cz.marvel.basedata.entities.Hero
import cz.marvel.data.Mapper
import cz.marvel.network.data.HeroDto

class HeroDtoToHeroMapper : Mapper<HeroDto, Hero> {
    override suspend fun map(from: HeroDto) =
        Hero(
            from.id,
            from.name,
            "${from.thumbnail.path}/landscape_amazing.${from.thumbnail.extension}",
            from.description
        )
}