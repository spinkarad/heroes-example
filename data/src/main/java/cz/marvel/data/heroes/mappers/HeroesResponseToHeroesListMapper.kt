package cz.marvel.data.heroes.mappers

import cz.marvel.basedata.entities.Hero
import cz.marvel.data.Mapper
import cz.marvel.data.forLists
import cz.marvel.network.data.HeroesResponse

class HeroesResponseToHeroesListMapper(private val heroDtoToHero: HeroDtoToHeroMapper) :
    Mapper<HeroesResponse, List<Hero>> {
    override suspend fun map(from: HeroesResponse) = heroDtoToHero.forLists(from.data.results)
}