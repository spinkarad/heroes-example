package cz.marvel.data.heroes

class DefaultHeroesRepository(
    private val dataSource: HeroesDataSource,
    private val dataStore: HeroesDataStore
) : HeroesRepository {

    override suspend fun observe() = dataStore.observe().also { refresh() }

    override suspend fun refresh() = dataSource.getData().let { dataStore.store(it) }
}