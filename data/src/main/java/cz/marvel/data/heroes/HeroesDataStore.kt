package cz.marvel.data.heroes

import cz.marvel.basedata.entities.Hero
import cz.marvel.data.ObservableDataStore
import cz.marvel.database.daos.HeroesDao
import kotlinx.coroutines.flow.Flow

class HeroesDataStore(private val dao: HeroesDao) : ObservableDataStore<List<Hero>> {

    override suspend fun observe(): Flow<List<Hero>> = dao.getHeroes()

    override suspend fun store(data: List<Hero>) =dao.insertAll(data)
}