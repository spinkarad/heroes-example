package cz.marvel.data.heroes

import cz.marvel.basedata.entities.Hero
import cz.marvel.data.DataSource
import cz.marvel.data.heroes.mappers.HeroesResponseToHeroesListMapper
import cz.marvel.network.RemoteService

class HeroesDataSource(
    private val remoteService: RemoteService,
    private val mapper: HeroesResponseToHeroesListMapper
) : DataSource<List<Hero>> {
    override suspend fun getData(): List<Hero> = remoteService.getHeroes().let { mapper.map(it) }
}