package cz.marvel.data

import kotlinx.coroutines.flow.Flow

interface ObservableDataStore<T> : DataObserver<T> {
    suspend fun store(data: T)
}

interface DataObserver<T> {
    suspend fun observe(): Flow<T>
}