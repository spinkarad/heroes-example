package cz.marvel.data.hero

import cz.marvel.basedata.entities.Hero
import cz.marvel.data.DataObserver
import cz.marvel.database.daos.HeroesDao
import kotlinx.coroutines.flow.Flow

class HeroDataStore(private val dao: HeroesDao, private val heroId: String) : DataObserver<Hero> {
    override suspend fun observe(): Flow<Hero> = dao.getHero(heroId)
}