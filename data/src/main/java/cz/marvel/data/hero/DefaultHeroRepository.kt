package cz.marvel.data.hero

import cz.marvel.basedata.entities.Hero
import kotlinx.coroutines.flow.Flow
import org.koin.core.parameter.parametersOf
import org.koin.java.KoinJavaComponent.inject

class DefaultHeroRepository : HeroRepository {


    override suspend fun observe(heroId: String): Flow<Hero> {
        val dataStore by inject(HeroDataStore::class.java) { parametersOf(heroId) }
        return dataStore.observe()
    }

}