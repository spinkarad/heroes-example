package cz.marvel.data.hero

import cz.marvel.basedata.entities.Hero
import kotlinx.coroutines.flow.Flow

interface HeroRepository {
    suspend fun observe(heroId: String): Flow<Hero>
}